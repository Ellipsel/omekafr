Le nouveau site [omeka.fr](https://omeka.fr) est basé sur le générateur de sites statiques [Hugo](https://gohugo.io/). Hugo prend en charge des fichiers de contenus dont le corps est rédigé en [Markdown](https://fr.wikipedia.org/wiki/Markdown) et les entêtes en [TOML](https://toml.io/fr/), ainsi que des fichiers de données au format [YAML](https://fr.wikipedia.org/wiki/YAML), pour l'agenda, et en [CSV]() pour les annuaires.  

Les contenus, données et les ressources nécessaires (css, javascripts, images, etc.) sont compilés pour produire des pages html à partir du thème [Compose](https://composedocs.netlify.app) et de ses modèles. 

## Gestion du dépôt Gitlab 

Nous gérons collectivement le site à partir du dépôt Gitlab : 

- https://gitlab.com/OmekaFR/omekafr

Le site lui même est hébergé sans frais sur le service des [pages statiques de Gitlab](https://docs.gitlab.com/ee/user/project/pages/) mis à jour via un [pipeline Gitlab](https://gitlab.com/OmekaFR/omekafr/-/pipelines). 

Il est nécessaire d'avoir un compte Gitlab pour éditer les pages et ajouter des contenus. 

L'édition s'effectue en local après avoir cloné le dépôt avec la commande `git` ou bien en ligne via l'éditeur de page ou l'environnement de développement propre à Gitlab.

Pour travailler en local, les logiciels `hugo` et `git` doivent êtres installés ainsi qu'un éditeur de texte.  

- créer le dépôt local :
  - cloner le dépôt : `git clone https://gitlab.com/OmekaFR/omekafr.git`
  - se déplacer dans le répertoire racine du site : `cd omekafr`
  - récupérer le thème compose : `git submodule update --init --recursive`
- lancer Hugo 
  - `hugo server`
  - entrer `Ctrl + C` pour l'arréter 
- éditer les fichiers de contenu
- synchroniser le dépôt local
  - `git -A`
  - `git commit -m "court commentaire sur les modifications et les ajouts"`
- mettre à jour le dépôt distant `git push` 
 
:zap: Attention ! chaque modification du dépôt (branche `master`) active le pipeline et régénère les pages.
      
## Arborescence de Hugo

Les principaux répertoires nécessaires à Hugo. En italique les répertoires de service, en gras ceux de contenus.

- _archetypes_ définition de type de contenus
- _assets_ ressources css et javascript
- _config_ fichiers de configuration
- **content** pages de contenus (entêtes TOML voire YAML et contenus en Markdown)
- **static** ressources associées aux contenus (images, etc.)
- **data** données structurées, le cas échéant
- _i18n_ fichiers des traductions 
- _themes_ thèmes du site

Une page peut être représentée par un fichier markdown ou par un répertoire comportant un fichier `_index.md`.

## Mise en page 

Fonctionnalités du thème [Compose](https://composedocs.netlify.app/docs/) utilisé par le site.

## Contenus 

### Organisation des contenus 

Pages spéciales, à la racine de `content/fr/` :

- `_index.md` page d'accueil             
- `contact.md` page de contact
- `infos-legales-cgu.md` informations légales, conditions générales d'utilisation et gestion des données personnelles.  
- `search.md` résultats de recherche, insérée dynamiquement au dessous du champ de recherche

Les contenus sont organisés en sections, les répertoires de premier niveau dans `content/fr/` :

- `aufo` vie statutaire de l'association (pages)
- `actions` activités de l'association (pages)             
- `actualites` billets horodatés
- `omeka` documentation 
- `csv` annuaires 

Les pages des sections listées dans le paramètre `docSections` possèdent un menu à gauche.

- `docSections = ["aufo","actions","omeka"]` (dans le fichier `config/_default/params.toml`)

                   
### Balises (Shortcodes)

#### Mise en forme

##### block

##### grid

##### column

##### button

##### picture

##### gallery

##### tabs / tab

##### tip

#### Flux 

##### actus

Liste des derniers billets d'actualité. 

- Utilisation :`{{< actus limite="2" >}}` 
- Paramètre : `limite` nombre de billets affichés.

##### agenda

Liste des dates de l'agenda.

- Utilisation : `{{< agenda limite="2" topo="ok" sorte="aufo" >}}` 
- Paramètres :
  - `limite` nombre de dates,
  - `topo` si égal à `ok` affiche la courte description,
  - `lieu` si egal à `ok affiche le lieu, lien si le lieu débute par http, 
  - `sorte` sélectionne les dates contenant cette valeur dans le champ `sorte`. 

##### annuaires

Liste les derniers annuaires publiés.

- Utilisation :`{{< annuaires >}}` 
- Paramètre : `limite` nombre d'annuaires.

#### Videos 

Vimeo, Youtube ou Dailymotion sont des services tiers qui installent des cookies. Tarte au citron gère, service par service, le consentememnt de l'usager. Il accepter ou refuser les cookies, si il les a refusé globalement il peut cependant un accepter au cas par cas en cliquant sur le bouton Àutosier`qui apparait à l'emplacement de la video. 

##### youtube

`{{< youtube "https://youtu.be/lK3FrBVu9KY" >}}` ou `{{< youtube "lK3FrBVu9KY" >}}`

##### vimeo

`{{< vimeo "https://vimeo.com/683973210" >}}` ou `{{< vimeo "683973210" >}}`

##### dailymotion

`{{< dailymotion "https://dai.ly/x8gqo04" >}}` ou `{{< dailymotion "x8gqo04" >}}`

#### Données et graphiques 

##### chart

[chart.js](https://www.chartjs.org/) permet d'afficher des données struturées d'un fichier csv sous la forme d'un graphique, d'un diagramme circulaire ou à barres, d'une table, etc. 

- [Documentation](https://composedocs.netlify.app/docs/compose/graphs-charts-tables/) 
- Utilisation : déclarer un jeu de données `dataset` dans l'entête de la page puis l'appeler à l'aide du shortcode `{{< chart "dataset" >}}` avec les paramètres _ad hoc_.

##### table 

Dérivé de `chart` pour afficher les annuaires csv spécifiques à OmekaFR. Cf. partie annuaire ci-après.

##### mermaid

[Mermaid](http://mermaid.js.org/) permet de générer des diagrammes (séquence, état, gantt, etc.) à partir de texte, dans le même style que Markdown. 

- [Documentation](https://composedocs.netlify.app/docs/compose/mermaid/) 
- Utilisation : `{{< mermaid >}} code Mermaid sur plusieurs lignes {{< /mermaid >}}`

### Images et fichiers

à caser dans les repertoires :

- images : `static/images`
- fichiers : `static/files`

quel système de nommage ?

### Page d'accueil

Localisation : `content/fr/_index.md`

### Pages

### Actualités (blog)

### Agenda

Les dates sont à insérer dans le fichier `data/agenda.yaml` dans le format [YAML](https://yaml.org/).

```
- titre: "InfOmeka : la gestion multisites" # titre (obligatoire)
  date: "2024-04-19 11:00" # date de début, avec ou sans horaire (obligatoire)
  fin: "2024-04-19 12:00" # date de fin, avec ou sans horaire (facultatif)
  topo: "Avec Pauline ... " # courte description (facultatif)
  url: "/fr/actions/gt/gt-dev/" # lien vers des infos supplémentaires (facultatif)
  lieu: "https://bbb.cnrs.fr/b/snb-b5q" # localisation, texte ou url pour une visio (facultatif)
  sorte: "aufo,GTdev" # mots clés destinés à trier les dates (facultatif)
```

Insérer les dates les plus récentes en haut du fichier. Il n'est pas indispensable de respecter l'ordre chronologique, il sera tout de même respecté à l'affichage.

### Annuaires

Les annuaires sont des pages de type csv localisées dans le répertoire `csv` situé à la racine. 

Entête d'une page d'annuaire.

```
+++
title = "Prestataires" 
summary = "Annuaire de prestataires Omeka"
draft = false
type = "csv"
[dataset]
  fileLink = "https://docs.google.com/spreadsheets/d/e/2PACX-1vQCMUnlOF0xLy_rxDX3Jl-_jZpprVp8EB_iXJagTjKFULoNNGPsyzsBb-A01jE4gxPFojDbbWv48VxP/pub?output=csv"
  columnTitles = ["nom","url","prestations"]  # liste des colonnes à afficher, la première est la colonne titre
  firstColumnURL = "2" # rang de l'url à associer à la colonne titre (dans le fichier de départ)
  title = "Prestataires"  # champ non encore affiché 
+++
```

Insérer ensuite le shortcode `{{< table "dataset" >}}` dans la page.

La liste complète des annuaires : https://omeka.fr/fr/csv/

## Gestion des cookies

Utilisation de [Tarte au citron](https://tarteaucitron.io/fr/)

## Formulaire de contact

Utilisation du service [Un-static](https://un-static.com). 

## Multilinguisme 

Le site est potentiellement multilingue, on peut donc insérer quelques pages de présentation en anglais et proposer les pages en français de Belgique, de Suisse, du Québec, du Sénégal, etc. :smiley: 

## Configuration et personnalisation

- fichiers de configuration :
  - configuration générale, à la racine `config.toml`
  - paramètres `config/_default/params.toml`
  - menus `config/_default/menus/menu.fr.toml`
  - multilinguisme `config/_default/languages.toml`
  - markdown `config/_default/markup.toml`
- surcharge des styles, 
- surcharge des modèles de contenus et de pages.


