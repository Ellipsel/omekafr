/**
 * Convert URLs in a string to anchor links
 * @param {!string} string
 * @returns {!string}
 * source : https://codepen.io/zuhairtaha/pen/NmbGKJ
 */

function URLify(string){
  const urls = string.match(/(\b(https?|ftp):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/gim);
  if (urls) {
    urls.forEach(function (url) {
      string = string.replace(url, '<a target="_blank" href="' + url + '">🔗</a>');
    });
  }
  return string.replace("(", "<br/>(");
}

const div = document.querySelectorAll("td.transformURL");

div.forEach(function (text) {
	text.innerHTML = URLify(text.innerText);
});
