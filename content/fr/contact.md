+++
title = "Contact"
featured_image = ""
omit_header_text = true
description = "Laissez-nous un message !"
type = "page"
draft = false
weight = 100
+++

<div class="contact">
<form method="post" action="https://forms.un-static.com/forms/c74e884ca900406de873960f20cf2b13c7602f00">
	<label>Votre nom</label>
       <input type="text" name="name" placeholder="Nom" required>
	<label>Votre courriel</label>
        <input type="email" name="email" placeholder="mail" required>
	<label>Votre message</label>
        <textarea name="message" placeholder="message" cols="40" rows="15"></textarea>
        <button type="submit" class="button"><span data-feather='send'></span>Envoyer</button>
</form>
        <p><small>(Formulaire géré par <a rel="nofollow" href="https://un-static.com/">Un-static</a>)</small></p>
</div>
