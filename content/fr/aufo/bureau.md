+++
title = "Bureau"
summary = "Bureau actuel et historique des anciens bureaux."
image = '/images/Victor_Hugo-Hunchback.jpg'
weight = 10
date = 2024-03-22
+++

## Bureau actuel
Bureau issu de l'assemblée générale du 13 décembre 2023.

* **Président**&nbsp;: Laurent Aucher (Université Paris Cité, Direction générale déléguée aux bibliothèques et musées, Paris)
* **Vice-président**&nbsp;: Pierre Willaime (CNRS, Archives Henri-Poincaré UMR7117, Nancy & Strasbourg)
* **Trésorier**&nbsp;: Richard Walter (Thalim, CNRS-ENS-Sorbonne nouvelle, Paris)
* **Trésorière adjointe**&nbsp;: Camille Desiles (CNRS, Les Afriques dans le monde - UMR5115, SciencesPo Bordeaux)
* **Secrétaire**&nbsp;: Camille Carette (Bibliothèque de l’École nationale des chartes, Paris)
* **Secrétaire adjoint**&nbsp;: Olivier Ghuzel (Université Paris Cité, Direction générale déléguée aux bibliothèques et musées, Paris)

Chargé·e·s de mission :
* **Chargé de mission "infrastructure"**&nbsp;: Thierry Pasquier (Espace Mendès France, CCSTI Poitiers-Nouvelle-Aquitaine)
* **Chargée de mission "GT-Développement"**&nbsp;: Elisa Thomas (Université PSL, Service Politique documentaire et Science ouverte, Paris)
* **Chargée de mission "inventaire des sites Omeka"**&nbsp;: Anne Garcia-Fernandez (CNRS, Litt&Arts - UMR 5316, Grenoble)

## Bureaux précédents

### 2023
Bureau issu de l'assemblée générale du 9 décembre 2022.

* **Présidente**&nbsp;: Anne Garcia-Fernandez
* **Vice-président**&nbsp;: Pierre Willaime
* **Trésorier**&nbsp;: Richard Walter
* **Trésorière adjointe**&nbsp;: Camille Desiles
* **Secrétaire**&nbsp;: Laurent Aucher
* **Secrétaire adjointe**&nbsp;: Camille Carette

Chargé·e·s de mission :
* **Chargé de mission "infrastructure"**&nbsp;: Thierry Pasquier
* **Chargée de mission "GT Développement"**&nbsp;: Elisa Thomas


### 2022
Bureau issu de l'assemblée générale du 29 novembre 2021.

* **Présidente**&nbsp;: Anne Garcia-Fernandez
* **Vice-président**&nbsp;: Thierry Pasquier
* **Trésorier**&nbsp;: Richard Walter
* **Trésorière adjointe**&nbsp;: Camille Desiles
* **Secrétaire**&nbsp;: Laurent Aucher
* **Secrétaire adjoint**&nbsp;: Pierre Willaime

Chargé·e·s de mission :
* **Chargée de mission "GT-Développement"**&nbsp;: Elisa Thomas

### 2021
Bureau issu de l'assemblée générale constitutive du 22 janvier 2021.

* **Présidente**&nbsp;: Anne Garcia-Fernandez
* **Vice-président**&nbsp;: Thierry Pasquier
* **Trésorier**&nbsp;: Richard Walter
* **Trésorier adjoint**&nbsp;: Julien Sicot
* **Secrétaire**&nbsp;: Laurent Aucher
* **Secrétaire adjoint**&nbsp;: Pierre Willaime