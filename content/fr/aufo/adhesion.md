+++
title = "Adhésion"
summary = "Tarifs et procédures d'adhésion pour les particuliers."
image = '/images/Victor_Hugo-Hunchback.jpg'
weight = 50
date = 2024-03-22
+++

Vous pouvez adhérer en ligne à notre association avec ce [formulaire HelloAsso](https://www.helloasso.com/associations/association-francophone-des-usagers-d-omeka/adhesions/adhesion-a-l-association-aufo/widget) sur laquelle nous gérons l'association. [Contactez nous](omekafr-bureau@groupes.renater.fr) si vous souhaitez ou si vous devez procéder autrement.

- Tarifs d’adhésion :  
  - individuelle : 20€
  - solidaire : 10€
  - entreprise / institution : 100€
  - adhésion de soutien : à partir de 150€

<div class="tac_helloasso" data-url="https://www.helloasso.com/associations/association-francophone-des-usagers-d-omeka/adhesions/adhesion-a-l-association-aufo/widget" width="100%" height="1200px"></div>

- Accès direct au [Formulaire d'adhésion et paiement en ligne](https://www.helloasso.com/associations/association-francophone-des-usagers-d-omeka/adhesions/adhesion-a-l-association-aufo/widget) sur HelloAsso.

