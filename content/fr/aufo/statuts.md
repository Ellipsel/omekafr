+++
title = "Statuts"
description = "Statuts de l'association des usagers francophones de Omeka."
summary = "Statuts de l'association des usagers francophones de Omeka."
featured_image = '/images/Victor_Hugo-Hunchback.jpg'
weight = 20
date = 2024-03-22
+++

## Article 1er : Constitution et dénomination

Il est fondé entre les soussignés et toutes personnes qui adhéreront aux présents statuts une association régie par la loi du 1er juillet 1901 et le décret du 16 août 1901, dénommée Association francophone des usagers d'Omeka (AUFO), regroupant les utilisateurs francophones d'Omeka (développé par le Roy Rosenzweig Center for History and New Media, Université George Mason, États-Unis) et ayant des projets scientifiques, culturels et/ou documentaires. Le siège social est fixé à l'Université Grenoble Alpes, CS 40700, 38058 Grenoble cedex 9. Il pourra être transféré par décision du Bureau.

## Article 2 : Objet

Cette association a notamment pour objet de :
- encourager l’entraide entre les utilisateurs francophones d'Omeka grâce au partage de l’information, de la documentation et des ressources ;
- entreprendre tout type d'action de valorisation et de formation pour promouvoir l'usage d'Omeka dans les communautés académiques, associatives et culturelles francophones ;
- se positionner comme un interlocuteur reconnu du Roy Rosenzweig Center for History and New Media et d’autres acteurs publics ou privés intéressés par Omeka ;
- appuyer les demandes de développement du logiciel Omeka répondant en particulier aux besoins des utilisateurs francophones ;
- fédérer toute initiative pour améliorer l'environnement francophone d'Omeka et ses différents outils ;
- et plus largement promouvoir l'utilisation des logiciels libres et l'ouverture des données culturelles et scientifiques.

Ces buts seront atteints par les moyens suivants :
- l'organisation de réunions, séminaires, journées d'études, ateliers, formations, etc. ;
- l'accompagnement à l'organisation d'événements en relation avec les objectifs de l'association ;
- la publication de travaux autour d'Omeka ;
- la constitution de groupes de travail thématiques ;
- la mise en commun de moyens, d’expérience et de savoir-faire ;
- le suivi ou le financement d'évolutions d'Omeka.

L’association pourra se consacrer à des activités économiques en relation avec ses objectifs.

## Article 3 : Durée

La durée de l’association est illimitée.

## Article 4 : Composition de l’association

L’association se compose de personnes physiques ou morales utilisatrices en accord avec les objectifs de l'association, ci-dessous dénommées « membres », à jour de leur cotisation. Pour être membre de l’association, une personne physique ou morale doit en faire la demande. Celle-ci doit être agréée par le bureau qui statue à chacune de ses réunions sur les demandes d’admission présentées et peut refuser une ou plusieurs d’entre elles sans être tenu de fournir ses motifs.

L’adhésion à l’association implique un engagement à participer à son fonctionnement et à sa gestion commune.

## Article 5 : Perte de la qualité de membre

La qualité de membre se perd par :
1. la démission adressée par écrit à la présidence de l’association,
2. la radiation pour non-paiement de la cotisation ou pour motif grave ou portant préjudice aux intérêts moraux ou matériels de l’association, apprécié par le Bureau. La personne membre, représentée par un.e délégué.e, aura été invitée huit jours avant par lettre recommandée ou courriel avec accusé réception à se présenter, physiquement ou par un quelconque moyen de visioconférence, devant le Bureau pour fournir des explications. Le Bureau prononcera le cas échéant la radiation. Un rapport sera présenté à l’Assemblée générale qui validera ou cassera la décision du Bureau. Une radiation est toujours prononcée lors de l’Assemblée générale.

## Articles 6 : Ressources

Les ressources de l’association comprennent :
1. Les cotisations, pour les personnes physiques d’une part, et pour les personnes morales d’autre part, dont le montant est fixé par l’Assemblée générale ;
2. Les subventions de l’État, des collectivités territoriales ou de tout autre organisme public ou privé ;
3. Toutes autres ressources autorisées par les textes législatifs ou réglementaires.

## Article 7 : Bureau

L’association est administrée par un Bureau d’au moins 4 et d’au plus 6 délégué·e·s, élu·e·s par l’Assemblée générale pour deux ans, et rééligibles. Le Bureau se compose d’un·e président·e, d’un·e vice-président·e, d’un·e trésorier·e et d’un·e secrétaire. S’il y a lieu, sur proposition du Bureau, un·e secrétaire adjoint·e et un·e trésorier·e adjoint·e peuvent être désigné·e·s. Le bureau doit refléter la diversité des membres de l'association. En cas de vacance, le Bureau pourvoit provisoirement au remplacement des délégués défaillants dans le respect de la diversité des membres de l'association. Il est procédé à leur remplacement définitif lors de l’Assemblée générale suivante.

## Article 8 : Pouvoirs du Bureau

Le Bureau est investi des pouvoirs nécessaires pour prendre toutes les décisions qui ne sont pas statutairement de la compétence de l’Assemblée générale. Il est chargé de la mise en œuvre des orientations décidées par l’Assemblée générale, ainsi que de l’ordre du jour de l’Assemblée générale, de la préparation des bilans et des propositions de modification des statuts. Il se prononce sur les admissions ou les exclusions des membres. Il peut confier des missions à l’un des membres de l’association.

Il rend compte de sa gestion lors de l’Assemblée générale.

## Article 9 : Fonctionnement du Bureau

Les membres du Bureau se doivent d’être en contact étroit pour administrer au mieux l’association, y compris par voie électronique.

Les décisions sont prises à la majorité des voix des membres du Bureau. En cas de partage égal des voix, la voix de la présidence compte double.

Les réunions du Bureau sont convoquées par courriel par les soins du ou de la président·e, assisté·e par le ou la secrétaire. Le Bureau ne peut valablement délibérer que si plus de la moitié de ses membres est présente. Les réunions sont présidées par la ou le président-e ou, par procuration, par le secrétaire si le président ne peut y assister.
Article 10 : Représentation de l'association
Le ou la président·e représente l’association dans tous les actes de la vie civile. Il a notamment qualité pour ester en justice au nom de l’association.

Il peut pour un acte précis déléguer ce pouvoir à un autre membre du Bureau.

## Article 11 : Assemblée générale

L’Assemblée générale comprend tous les membres de l’association en règle de cotisation. Chaque membre peut se faire représenter par un·e autre membre de son choix. Le nombre de pouvoirs est limité à deux par membre présent. 

L’Assemblée générale est seule compétente pour :
- élire, renouveler et révoquer les membres du Bureau ;
- modifier les statuts et prononcer la dissolution de l’association ;
- contrôler la gestion du Bureau, la situation financière et morale de l’association.

## Article 12 : Fonctionnement de l’Assemblée générale

Elle se réunit, en présentiel ou en visioconférence, au moins une fois par an et chaque fois qu’il est besoin, soit sur convocation de la présidence, soit sur demande du tiers des membres de l’association. Quinze jours au moins avant la date prévue, les membres de l’association sont convoqués, par écrit, par les soins de la ou du président, assisté·e dans cette tâche par le ou la secrétaire. L’ordre du jour, établi par le Bureau, est indiqué sur les convocations.

Toute proposition de modification de l’ordre du jour devra être déposée huit jours au moins avant la réunion et sera soumise à l’Assemblée. Le ou la présidente, assisté·e des membres du Bureau, préside l’Assemblée et expose la situation morale de l’association. La ou le trésorier·e rend compte de sa gestion et soumet le bilan annuel à l’approbation de l’Assemblée.

Les décisions de l’Assemblée générale statuant à titre ordinaire sont prises à la majorité absolue des membres présents ou représentés. Toutes les décisions ordinaires sont prises à main levée, mais peuvent se prendre à bulletin secret à la demande d’un·e des membres. Le vote à main levée par visioconférence est considéré comme valable ; s'il y a besoin d'un vote à bulletin secret et que l'assemblée générale ne peut se dérouler en présentiel, un système de vote garantissant l'anonymat devra être mis en place.

L’Assemblée générale ne peut valablement délibérer que si un tiers au moins de ses membres est présent ou représenté. Si le quorum n’est pas atteint, une nouvelle Assemblée générale peut être convoquée dans un délai de 15 jours minimum sans nécessité de quorum.

Si elle doit statuer sur la révocation d’un ou des membres du Bureau, l’Assemblée générale, ordinaire ou extraordinaire, ne peut valablement délibérer que si la moitié de ses membres est présente ou représentée et prend ses décisions à la majorité qualifiée des deux tiers des membres présents ou représentés.

## Article 13 : Dissolution

La dissolution peut être prononcée lors d’une Assemblée générale. Cette dernière est à prononcer par au moins les deux tiers des membres présents ou représentés, l’Assemblée générale désigne deux personnes chargées de procéder aux opérations de liquidation. L’actif net, s’il y a lieu, est dévolu, par cette Assemblée, à une ou plusieurs associations régies par la loi du 1er juillet 1901 et le décret du 16 août 1901 poursuivant des buts similaires.

## Notes 
  
Statuts adoptés par l'assemblé générale constitutive du 22 janvier 2021.
      
