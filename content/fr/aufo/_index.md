+++
title = "L'association"
description = "L'association des usagers francophone d'Omeka (AUFO), regroupe les utilisateurs francophones des logiciels Omeka."
image = '/images/Victor_Hugo-Hunchback.jpg'
weight = 10
draft = false
date = 2024-03-20
+++

L'Association des usagers francophones de Omeka a été créée le 21 septembre 2021. Elle trouve son origine dans un certain nombre d'initiatives.

Ses objectifs sont les suivants :

* encourager l’entraide entre les utilisateurs francophones d'Omeka grâce au partage de l’information, de la documentation et des ressources ;
* entreprendre tout type d'action de valorisation et de formation pour promouvoir l'usage d'Omeka dans les communautés académiques, associatives et culturelles francophones ;
* se positionner comme un interlocuteur reconnu du Roy Rosenzweig Center for History and New Media et d’autres acteurs publics ou privés intéressés par Omeka ;
* appuyer les demandes de développement du logiciel Omeka répondant en particulier aux besoins des utilisateurs francophones ;
* fédérer toute initiative pour améliorer l'environnement francophone d'Omeka et ses différents outils ;
* et plus largement promouvoir l'utilisation des logiciels libres et l'ouverture des données culturelles et scientifiques.

Le groupe de discussion omekafr-asso@groupes.renater.fr est ouvert à toutes et tous, adhérent·e·s ou pas.


