+++
title = "Sites sous Omeka"
summary = "Annuaire collaboratif de sites fonctionnant avec Omeka Classic ou Omeka S"
type = "csv"
draft = false
weight = 10
[dataset]
  fileLink = "https://docs.google.com/spreadsheets/d/e/2PACX-1vRmJyQluZsmvoMSQwu1f7S77k6Huu8dvVLxWy9iDv9Atnc-ce4zENAiVwgtU50Ym_77oo-uBVFDgAty/pub?gid=137364925&single=true&output=csv"
  columnTitles = ["Site","Omeka","url"]
  title = "Sites sous Omeka S" 
+++

Vous pouvez déclarer votre site en complétant ce [formulaire en ligne](https://docs.google.com/forms/d/e/1FAIpQLSexxqVoHfdKIYXxy_anDyLYHKtSfM42yVtZ69cNk3_Idgzc0g/viewform?usp=sf_link). Auparavant, merci de vérifier qu'il n'existe pas dans la liste ci-après. N'oubliez pas non plus de faire connaitre votre réalisation sur le site [omeka.org](https://docs.google.com/forms/d/e/1FAIpQLSdWS_9FS0RgpnPQfXRq2BZvM-SLJ9u-frrp3PY3-FlXOUoK6Q/viewform).

{{< table "dataset" >}}
