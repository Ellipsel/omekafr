+++
title = "Biblio Omeka avec Zotero"
summary = "Comment récupérer les items d'un groupe Zotero, par exemple la Biblio Omeka Francophone."
type = "csv"
draft = false
weight = 10
[dataset]
  fileLink = "https://docs.google.com/spreadsheets/d/e/2PACX-1vS4gylyGpQmoiOocd8gCgIUIe1_uV2Ea5Y2mOONVt5SZNG24ZTWm-hSgoTXd87kasCfwpiO4JDgBhXL/pub?output=csv"
  columnTitles = ["Title","Author","Url"]
  title = "Biblio Zotero"
+++

Issue du groupe Zotero [Omeka Biblio francophone](https://www.zotero.org/groups/2382399/omeka_biblio_francophone/library)

Mais en passant par Google sheet car il y un bug dans la récupération directe par Hugo du csv via l'API Zotero. 

Dans Google Sheets, récupérer le fichier dans la case A1 en passant la formule :

- `=IMPORTDATA("https://api.zotero.org/groups/2382399/items?key=XYZ&format=csv")`

La synchronisation est automatique. 

{{< table "dataset" >}}
