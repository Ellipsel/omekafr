+++
title = "Prestataires"
summary = "Annuaire de prestataires Omeka"
draft = false
type = "csv"
[dataset]
  fileLink = "https://docs.google.com/spreadsheets/d/e/2PACX-1vQCMUnlOF0xLy_rxDX3Jl-_jZpprVp8EB_iXJagTjKFULoNNGPsyzsBb-A01jE4gxPFojDbbWv48VxP/pub?output=csv" 
  columnTitles = ["nom","prestations"]
  firstColumnURL = "2"
  title = "Prestataires"
+++

{{< table "dataset" >}}

