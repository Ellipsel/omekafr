+++
title = "Modules Omeka S"
summary = "Annuaire collaboratif des modules Omeka S. Cette liste n'est pas exhaustive mais elle est régulièrement mise à jour par les membres du GT DEV"
type = "csv"
draft = false
[dataset]
  fileLink = "https://docs.google.com/spreadsheets/d/e/2PACX-1vRdhM7DV2aNTJMML6DB3ga8ldo2UHP-r_CulMlL8PVhbJZUkly7g_q2V5p4zTQzOIYAdelKWIFL3tSb/pub?gid=0&single=true&output=csv"
  columnTitles = ["Nom","Statut","Type","Description"]
  firstColumnURL = "2"
  title = "Modules Omeka S"
+++

{{< table "dataset" >}}

