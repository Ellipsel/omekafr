+++
title = "Journées 2022 - Grenoble"
date = 2023-07-23T15:51:53+02:00
image = ""
type = "date"
description = "Rencontres annuelles des usagers francophones de Omeka."
date_debut = "8 - 9 décembre 2023"
date_fin = "8 - 9 décembre 2023"
lieu = "Université Grenoble Alpes"
draft = false
weight = 20
+++

## Omeka - Échanges & Réutilisations

Les Journées annuelles Omeka 2022 ont été organisées par l’association des usagers francophones d’Omeka (AUFO) les 8 et 9 décembre à l’Université Grenoble Alpes.

Ces journées ouvertes à toutes et tous ont été l'occasion de participer à des ateliers thématiques et des conférences autour du thème « Échanges & Réutilisations » et de faire le point sur les travaux des groupes de travail de l'association.

Vous pouvez consulter le [programme détaillé](https://omeka.sciencesconf.org/resource/page/id/12).