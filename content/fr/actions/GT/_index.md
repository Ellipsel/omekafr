+++
title = "Groupes de travail"
summary = "Présentation des GT de l'association"
weight = 30
draft = false
date = 2017-03-02T12:00:00-05:00
+++

Les adhérent(e)s de l'association ont la possibilité de se réunir en groupes de travail sur certains sujets spécifiques liés à Omeka Classic et Omeka S. Les réunions peuvent avoir lieu en visioconférence tout au long de l'année, ainsi qu'en présentiel lors des Journées annuelles organisées par l'association. Une occasion de travailler de manière plus conviviale !

La coordination de ces groupes n'est pas nécessairement assurée par un(e) membre du bureau. Ce dernier, cependant, peut mettre à disposition des GT une adresse mail dédiée, un groupe Renater, etc.

Si vous souhaitez participer à la vie active de l'association en animant un GT, n'hésitez pas à vous signaler : omekafr-bureau@groupes.renater.fr !