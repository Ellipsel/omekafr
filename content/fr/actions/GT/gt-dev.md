+++
title = "GT Dev"
summary = "Activités du groupe de travail développement."
date = 2017-04-09T10:58:08-04:00
image = "/images/Pope-Edouard-de-Beaumont-1844.jpg"
+++
Le GT-Dev, ou groupe de travail consacré aux développements, est animé par Élisa Thomas (Université PSL).

## Objectifs
* Cartographier les fonctionnalités existantes ;
* Evaluer et tester les modules disponibles, identifier les documentations afférentes ;
* Cibler l’état des développements, en cours et à venir, à terme, porter des développements mutualisés ;
* Echanges et conseils techniques et fonctionnels.

Une question ? Envie de participer ? Participez aux réunions régulières du GT-Dev en écrivant à omekafr-gt-dev@groupes.renater.fr

## InfOmeka
Le GT-Dev organise également des sessions mensuelles d'information et d'échanges autour des fonctionnalités d'Omeka S lors des rendez-vous "InfOmeka" ouverts à tous. Afin de permettre des échanges libres et spontanés, ces sessions ne sont pas enregistrées. Le programme des sessions "InfOmeka" est régulièrement annoncé sur la liste générale de diffusion de l'AUFO : omekafr-asso@groupes.renater.fr.

## Agenda
{{< agenda sorte="GTdev" topo="ok" >}}
