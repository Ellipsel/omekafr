+++
title = "GT Formation & documentation"
summary = "Activités du groupe de travail formation et documentation."
date = 2017-04-09T10:58:08-04:00
image = "/images/Pope-Edouard-de-Beaumont-1844.jpg"
+++

Le GT Formation et documentation est animé par Sylvie Sallé (Université Paris-Saclay).

Objectifs :
* Recenser les formations et la documentation sur Omeka Classic et Omeka-S ;
* Analyser les besoins en formations et documentations des usagers d’Omeka ;
* Proposer des actions adaptées.

Une question ? Envie de participer ? Contactez **(CREER UNE ADRESSE)**

## Agenda du GT formation & documentation

{{< agenda sorte="GTformation&documentation" topo="ok" >}}
