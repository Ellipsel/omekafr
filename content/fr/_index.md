+++
title = "Association des usagers francophones de Omeka"
summary = "Site des événements et des ressources de la communauté des usagers francophone des logiciels Omeka"
+++

# Communauté des usagers francophones de Omeka

Omeka Classic et Omeka S sont des logiciels libres de gestion et de publication de fonds d’archives, de corpus scientifiques et de collections de documents ou d’objets.

{{< actu-une >}}

{{< block "grid-3 mb-3" >}}
{{< column >}}

{{< tip "info" "vide">}}

<h2>information</h2> 

ne perdez rien des nouveautés de Omeka et de ses actualités, retrouvez ici toutes les informations utiles à vos projet

{{< /tip >}}

{{< /column >}}

{{< column >}}

{{< tip "warning" "vide" >}}

<h2>partage</h2>

échangez vos réflexions, expériences, astuces et savoir-faire, participez aux groupes de travail 

{{< /tip >}}

{{< /column >}}

{{< column >}}

{{< tip "danger" "vide" >}}

<h2>entraide</h2>

ne restez pas bloqué ! adressez vous à la communauté, trouvez ici des partenaires et des pistes de coopération 

{{< /tip >}}

{{< /column >}}

{{< /block >}}

<section class="grid-2" >
<div>

### Gazouillis X

<span class="tacTwitterTimelines"></span><a class="twitter-timeline" href="https://twitter.com/omeka_fr" data-tweet-limit="3" data-dnt="dnt true" data-width="100%" data-height="500px" data-theme="theme light" data-link-color="hex #769890"></a>

{{< agenda limite="6" >}}

{{< button "/fr/actions/evenements/" "toutes les dates" >}}

</div>

<div>

{{< actus limite="2" >}}

### les annuaires

{{< annuaires >}}

{{< button "/fr/csv/" "tous les annuaires" >}}

</div>
</section>


