+++
title = "Omeka S"
summary = "Présentation de Omeka S."
weight = 20
draft = false
+++

Omeka S est une nouvelle variante du logiciel Omeka qui vient compléter plutôt que remplacer Omeka Classic. Bien que les principes et objectifs soient les mêmes, Omeka S a été complètement réécrit. Cette version est plus particulièrement destinée aux institutions qui gèrent plusieurs sites et qui souhaitent publier des données liées (linked data). La conception du logiciel a été initiée en 2012, sept versions alpha (2015-2016) et quatre versions bêta (2016-2017) ont été nécessaires avant d'aboutir à la version stable 1.0.0 en novembre 2017.

*Version en cours* : 4.0.4 (25 août 2023) téléchargement et notes de version

Des changements importants et des fonctionnalités nouvelles se trouvent dans la gestion et les paramétrages des blocs de contenus et des thèmes des sites (pages, navigations, collectes des items, tris, etc.).

Au plan technique, cette version ajoute la prise en charge de PHP 8.1 et 8.2. La version minimale requise de PHP est désormais 7.4. Elle bénéficie aussi de nombreuses corrections de bugs et améliorations. Vous pouvez en trouver la liste à partir de la page de téléchargement.

Versions antérieures

**Principales caractéristiques d'Omeka S :**

<u>Multisite</u>

* Gestion d'un nombre illimité de sites
* Rôles : superadmin, administrateur des sites (CMS), administrateur des données, auteur, ...

<u>Focus sur l'échange de données</u>

* Implémente certains principes du web de données (format natif JSON-LD, URI, description des ressources à l'aide de vocabulaires RDF)
* Interoperabilité avec d'autres systèmes (connecteurs Fedora, DSpace, Zenodo ; API REST)

<u>Wiki Omeka S</u> : https://github.com/omeka/omeka-s/wiki

<u>Documentation</u> : http://omeka.org/s/docs/user-manual

<u>Une sandbox permet de découvrir le logiciel</u> : https://omeka.org/s/download/#sandbox

<u>Thèmes</u> : https://github.com/omeka-s-themes

<u>Modules développés par Omeka.org</u> : https://github.com/omeka-s-modules

<u>Une table de correspondance permet de suivre le portage des plugins Omeka Classic en modules Omeka S</u> : https://daniel-km.github.io/UpgradeToOmekaS/
