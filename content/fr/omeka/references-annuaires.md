+++
title = "Références et annuaires"
summary = "Références et annuaires de ressources relatives à Omeka Classic et à Omeka S"
weight = 40
draft = false
+++

voir la [liste des annuaires](/fr/csv)


Les cours et les tutoriels sont indiqués dans Projets & ressources
Omeka

* Omeka.org
* Plugins (Omeka Classic) : https://omeka.org/classic/plugins
* Modules (Omeka S) : https://github.com/omeka-s-modules
* Mapping plugins / modules : https://daniel-km.github.io/UpgradeToOmekaS/
* Thèmes Omeka Classic : https://omeka.org/classic/themes
* Thèmes Omeka S : https://github.com/omeka-s-themes
* Page [Wikipedia](https://fr.wikipedia.org/wiki/Omeka) Omeka
* Page [Bibliopedia](http://www.bibliopedia.fr/wiki/Omeka) Omeka

Liste de diffusion / discussion

* RRCHNM : https://forum.omeka.org
* AUFO : https://groupes.renater.fr/sympa/info/omekafr-asso


Hébergement

* [Omeka.net](https://www.omeka.net/)
* [Huma-Num](https://www.huma-num.fr/les-services-par-etapes/)

Prestataires (liste donnée à titre indicatif)

* [Biblibre](http://www.biblibre.com/fr)
* [Limonade & Co](http://www.limonadeandco.fr/)
* [Numerizen](http://omeka.numerizen.com/)TJS d'actualité ?
* [Progilone]TJS d'actualité ? 
* [Sempiternelia](https://sempiternelia.net/)

 Certains prestataires proposent une offre d'hébergement.
 
Bibliographie :

* Building a digital archive for the preservation of local memories (with Omeka S), at the intersection of public history and digital history, Giorgio Comai, billet de blog. https://giorgiocomai.eu/post/2020-12-31-ecomuseo-digital-archive-with-omeka-s/ (dernière mise à jour 31/12/2020).
* Omeka Classic, Elina Leblanc (ed.), 2019. https://ride.i-d-e.de/issues/issue-11/omeka/ (dernier accès : 21/08/2020).
* An Omeka S Repository for Place- and Land-Based Teaching and Learning, Neah Ingram-Monteiro, Ro McKernan, 2022. https://ejournals.bc.edu/index.php/ital/article/view/15123 (dernier accès : 22/02/2023).
