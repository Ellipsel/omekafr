+++
title = "Omeka Classic"
summary = "Présentation de Omeka Classic."
weight = 10
draft = false
+++

Omeka Classic est le nom donné à la branche originelle de Omeka depuis la sortie de Omeka S, branche orientée web sémantique. 

Omeka Classic est un logiciel libre de gestion et de valorisation de fonds d’archives et de collections de documents ou d’objets. Il est destiné aux bibliothèques, médiathèques, musées, écomusées, sociétés savantes ou historiques, organismes de recherche, établissements d’enseignement, groupe de citoyens, etc. désireux de partager leurs ressources ou d’en collecter de nouvelles.

Gérant ensemble notices de métadonnées et fichiers associés (textes, images, sons, etc.), il se situe au croisement de différents besoins : publication de contenus, gestion de collections et éditorialisation des ressources.

Il est basé sur une communauté de développeurs et d’utilisateurs venant des bibliothèques, des musées et de la recherche. Omeka s'attache à respecter les recommandations du W3C, les normes 508 d'accessibilité et les standards de métadonnées (Dublin Core par défaut).

Il propose une API accessible en lecture comme en écriture.

[Site officiel](https://omeka.org/classic/)

La version 3.0 a été publiée le 22 septembre 2021.

Consulter le répertoire des sites Omeka Classic : https://omeka.org/classic/directory/
