+++
title =  "À propos d'Omeka"
summary =  "À propos d'Omeka"
image =  '/images/Victor_Hugo-Hunchback.jpg'
weight = 40
draft =  false
+++

Ce système de publication web spécialisé dans l’édition de collections muséales, de bibliothèques numériques et d’éditions savantes en ligne se situe à la croisée du système de gestion de contenus (CMS) de la gestion de collections patrimoniales ainsi que de l’édition d'archives numériques. Son développement est désormais assuré par la [Corporation for Digital Scholarchip](https://digitalscholar.org/), qui a également développé les logiciels Zotero et Tropy. Le but des créateurs de cet outil, facile à prendre en main, est de permettre aux utilisateurs de se concentrer sur le contenu et sur l’interprétation plutôt que sur la programmation.

Omeka permet en effet de publier des contenus de façon très simple et très flexible. Construite avec les langages informatiques les plus répandus du web (PHP, Javascript, HTML/CSS), son architecture de base est épurée ; elle peut alors s’adapter selon les besoins de chaque projet grâce à l’ajout de modules (plugins) et à l’utilisation de modèles visuels (templates) réunis en thèmes génrériques et/ou personnalisables.

Le logiciel est diffusé en licence libre, compatible pour un environnement LAMP (Linux Apache, MySQL, PHP). Le site officiel [Omeka](https://omeka.org/) distribue deux versions : 
- [Omeka Classic](https://beta.omeka.fr/fr/omeka/omeka-classic/), la version initiale, propose un schéma de bibliothèque numérique complet : gestion des documents et des collections, suivant des descriptions en Dublin Core, création de pages et de parcours virtuels publiques sur un site dédié. Sa prise en main rapide permet de monter facilement des projets individuels. 
- [Omeka S](https://beta.omeka.fr/fr/omeka/omeka-s/), version développée plus récemment, reprend les mêmes fonctionnalités tout en suivant les standards du web sémantique. Elle a notamment la particularité de permettre une diffusion multi-sites des ressources. 

Les deux solutions logicielles évoluent régulièrement et proposent au fur et à mesure des montées en version de nouvelles fonctionnalités. 
Les évolutions d'Omeka Classic et d'Omeka S ainsi que les développements de modules optionnels sont assurés par une large communauté de développeurs et d’utilisateurs venant de l’informatique, des bibliothèques, des musées et des sciences humaines.
