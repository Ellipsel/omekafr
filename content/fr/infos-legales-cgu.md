+++
title = "Informations légales - CGU"
summary = "Informations légales, conditions d'utilisation et politique de gestion des données personnelles du Site de l'AUFO"
weight = 10
draft = false
date = 2024-03-29
+++

Le site omeka.fr est édité par l’association des usagers francophones d’Omeka (AUFO) Il est géré d’une façon collégiale par les membres de l’association ainsi que par les contributeurs et contributrices.

- **Adresse** : Association Francophone des Usagers d’Omeka, Université Grenoble Alpes, CS 40700 - 621 avenue Centrale, 38400 Saint-Martin-d’Hères.
- **Responsables de publication** : la présidente ou le président de l'association, se reporter à la [composition du bureau](about/asso).
- **Hébergement** GitLab, Inc., 268 Bush Street #350 San Francisco (USA) représentée par GitLab France SASn 57 Rue d'Amsterdam, 75008 Paris 8. 

## Contact

Vous pouvez nous contacter en utilisant le [formulaire de contact de ce Site](http://omeka.fr/contact)

## Conditions générales d’utilisation (CGU)

Les présentes conditions générales d’utilisation (dites « CGU ») ont pour objet l’encadrement juridique des modalités de mise à disposition du site `omeka.fr`, ci après dénommé le Site, et de définir les conditions d’accès et d’utilisation par « l’Utilisateur ». 

Toute utilisation et participation au Site implique l’acceptation sans aucune réserve ni restriction des présentes CGU par l’utilisateur. 

En cas de non-acceptation des CGU stipulées dans le présent contrat, l’Utilisateur se doit de renoncer à l’accès au Site de l'AUFO.

L'AUFO se réserve le droit de modifier unilatéralement et à tout moment le contenu des présentes CGU.

### Art. 1 : Accès au Site

Omeka.fr est le Site de la communauté des usagers francophones de Omeka, il publie des informations sur l'actualité de la vie statutaire de l'association et ses activités, des ressources sur Omeka et ses usages, des nouvelles d'intérêt général pour la communauté, etc. 

L'alimentation et l'entretien du Site s'effectue de façon collaborative par les membres de l'association et les personnes intéressées. Pour ce faire, il peut être nécessaire de s'inscrire et d'accepter les CGU de services tiers, notamment Gitlab et Google.

Le Site est accessible en tout lieu à tout Utilisateur ayant un accès à Internet. Tous les frais supportés par l’Utilisateur pour accéder au service (matériel informatique, logiciels, connexion Internet, etc.) sont à sa charge.

Tout événement dû à un cas de force majeure ayant pour conséquence un dysfonctionnement du Site ou serveur et sous réserve de toute interruption ou modification en cas de maintenance, n’engage pas la responsabilité de l’AUFO. Dans ces cas, l’Utilisateur accepte ainsi ne pas tenir rigueur à l’éditeur de toute interruption ou suspension de service, même sans préavis.

L’Utilisateur a la possibilité de contacter l’éditeur via le [formulaire de contact](/contact) ou par messagerie électronique.

### Art. 2 : Collecte des données

Le Site lui-même ne requiert aucune ouverture de compte par les Utilisateurs et il ne collecte aucune donnée personnelle concernant les Utilisateurs.

Un [formulaire de contact](/contact) nous permet de recevoir, gérer et répondre aux questions et remarques des Usagers. Ce formulaire collecte les données personnelles suivantes : nom, courriel. Le renseignement de ce formulaire est libre et basé sur le consentement. Le responsable de ce traitement est l’AUFO.

Conformément à la Loi Informatique et Libertés et le Règlement général sur la protection des données (RGPD), vous disposez d’un droit d’accès, de modification, de suppression et d’opposition au traitement de vos données à caractère personnel. Pour faire valoir ce droit ou pour tout autre demande relative à la gestion des données personnelles, adressez un message à rgpd[@]omeka.fr ou via le [formulaire du Site](/contact).

Le service de contact est opéré par le service Un-static dont le siège social est situé : Prinses Margrietplantsoen 33, 2595 AM, The Hague (Pays-Bas).

### Art. 3 : Propriété intellectuelle

Les marques et logos, ainsi que tous les contenus du Site (textes, images, son, etc.) font l’objet d’une protection par le Code de la propriété intellectuelle et plus particulièrement par le droit d’auteur.

Les contenus spécifiques à ce Site sont publiés sous licence Creative commons Attribution - Partage dans les Mêmes Conditions. Les contenus externes utilisés sont publiés selon leurs licences propres. L’Utilisateur doit solliciter l’autorisation préalable du Site pour toute reproduction, publication, copie des contenus dont les modalités de diffusion ne sont pas connues ou non libres.

L’Utilisateur s’engage à ne pas utiliser le Site à des fins commerciales ou publicitaires.

Toute représentation totale ou partielle de ce Site par quelque procédé que ce soit, sans l’autorisation expresse de l’exploitant du Site Internet constituerait une contrefaçon sanctionnée par l’article L 335-2 et suivants du Code de la propriété intellectuelle.

Il est rappelé conformément à l’article L122-5 du Code de propriété intellectuelle que l’Utilisateur qui reproduit, copie ou publie le contenu protégé doit citer l’auteur et sa source.

### Art. 4 : Responsabilité

Les sources des informations diffusées sur le Site sont réputées fiables mais le Site ne garantit pas qu’il soit exempt de défauts, d’erreurs ou d’omissions.

Les informations communiquées sont présentées à titre indicatif et général sans valeur contractuelle. Malgré des mises à jour régulières, le Site ne peut être tenu responsable de la modification des dispositions administratives et juridiques survenant après la publication. De même, le Site ne peut être tenu responsable de l’utilisation et de l’interprétation de l’information contenue dans ce Site.

Le Site ne peut être tenu pour responsable d’éventuels virus qui pourraient infecter l’ordinateur ou tout matériel informatique de l’Internaute, suite à une utilisation, à l’accès, ou au téléchargement provenant de ce Site.

La responsabilité du Site ne peut être engagée en cas de force majeure ou du fait imprévisible et insurmontable d’un tiers.

### Art. 5 : Liens hypertextes

Des liens hypertextes peuvent être présents sur le Site. L’Utilisateur est informé qu’en cliquant sur ces liens, il sortira du Site. Ce dernier n’a pas de contrôle sur les pages web sur lesquelles aboutissent ces liens et ne saurait, en aucun cas, être responsable de leur contenu.

### Art. 6 : Cookies

L’Utilisateur est informé que pour le suivi de la fréquentation du Site un cookie peut s’installer automatiquement sur son logiciel de navigation.

Les cookies sont de petits fichiers stockés temporairement sur le disque dur de l’ordinateur de l’Utilisateur par votre navigateur et qui sont nécessaires à l’utilisation du Site. Les cookies ne contiennent pas d’information personnelle et ne peuvent pas être utilisés pour identifier quelqu’un. Un cookie contient un identifiant unique, généré aléatoirement et donc anonyme. Certains cookies expirent à la fin de la viSite de l’Utilisateur, d’autres restent.

L’information contenue dans les cookies est utilisée pour collecter les information sur la fréquentation du Site.

En naviguant sur le Site, L’Utilisateur les accepte.

L’Utilisateur doit toutefois donner son consentement quant à l’utilisation de certains cookies.

A défaut d’acceptation, l’Utilisateur est informé que certaines fonctionnalités ou pages risquent de lui être refusées. L’Utilisateur pourra désactiver ces cookies par l’intermédiaire des paramètres figurant au sein de son logiciel de navigation.

Les informations suivantes générées par Matomo (le logiciel de suivi utilisé par le Site) sont relatives à la configuration de votre navigateur : 

<div id="matomo-opt-out"></div>
<script src="https://stats.emf.fr/index.php?module=CoreAdminHome&action=optOutJS&divId=matomo-opt-out&language=auto&fontFamily=Barlow&showIntro=1"></script>
      

### Art. 7 : Droit applicable et juridiction compétente

La législation française s’applique au présent contrat. En cas d’absence de résolution amiable d’un litige né entre les parties, les tribunaux français seront seuls compétents pour en connaître.

Pour toute question relative à l’application des présentes CGU, vous pouvez joindre l’éditeur aux coordonnées inscrites au début de ce document ou via le [formulaire de contact](/contact). 

### Révisions 

- 29 mars 2024 - version initiale.

