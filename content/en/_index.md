+++
title = "Association des usagers francophones de Omeka"
summary = "Site des événements et des ressources de la communauté des usagers francophone des logiciels Omeka"
+++

<section class="grid-2" >
<div>

# Le site des usagers francophones de Omeka

Omeka Classic et Omeka S sont des logiciels libres de gestion et de publication de fonds d’archives, de corpus scientifiques et de collections de documents ou d’objets.

{{< tip "warning" >}}
ne perdez-rien de l'actualité autour de Omeka
 {{< /tip >}}

{{< tip >}}
une source d'informations 
{{< /tip >}}

{{< button "/actions/" "retrouvez toutes nos actions" >}}

</div>

<div>

![omeka](/images/gohugo-default-sample-hero-image.jpg)

{{< agenda limite="3" topo="ok" >}}

</div>
</section>


